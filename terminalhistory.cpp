#include "terminalhistory.h"

TerminalHistory::TerminalHistory(QObject *parent) : QObject(parent)
{
	histIndex = 0;
	QSettings settings (QSettings::UserScope, "groundST", "history");
	if (settings.contains ("history"))
		cmdHistory.append (settings.value ("history").toStringList ());

	if (!settings.contains ("historySize"))
		settings.setValue ("historySize", "100");

	historySize = settings.value ("historySize").toInt ();
}

TerminalHistory::~TerminalHistory()
{
}

void TerminalHistory::addToHistory (QString cmd)
{
	if (!cmd.isEmpty())
	{
		if (cmdHistory.length() < historySize)
			cmdHistory.append (cmd);
		else
		{
			cmdHistory.removeFirst ();
			cmdHistory.append (cmd);
		}
		histIndex = 0;
		QSettings settings (QSettings::UserScope, "groundST", "history");
		settings.setValue ("history", cmdHistory);
	}
}

QString TerminalHistory::nextOnCmdHistory ()
{
	QString ret;
	histIndex--;
	if (histIndex < 0)
	{
		histIndex = cmdHistory.length() - 1;
		ret = cmdHistory.at (histIndex);
	}
	else
	{
		ret = cmdHistory.at (histIndex);
	}
	return ret;
}

QString TerminalHistory::previousOnCmdHistory ()
{
	QString ret;
	histIndex++;
	if (histIndex >= cmdHistory.length())
	{
		histIndex = 0;
		ret = cmdHistory.at (histIndex);
	}
	else
	{
		ret = cmdHistory.at (histIndex);
	}
	return ret;
}

QStringList TerminalHistory::listHistory ()
{
	return cmdHistory;
}
