#ifndef PORTCONFIG_H
#define PORTCONFIG_H

#include <QDialog>
#include <QSettings>
#include <QFile>
#include <QDir>
#include <QtSerialPort/QSerialPortInfo>
#include <QIntValidator>
#include "port.h"

class Port;

namespace Ui
{
	class PortConfig;
}

	class PortConfig : public QDialog
	{
		Q_OBJECT

	public:
		explicit PortConfig(QWidget *parent = 0, Port *p = 0);
		~PortConfig();

	private slots:
		void on_buttonBox_accepted ();
		void showPortInfo (int idx);

	private:
		Port *uart;
		Ui::PortConfig *ui;
		pSettings ps;

		void fillPortsInfo ();
		void fillPortsParameters();

		void loadSavedPortSettings ();
		void savePortSettings (pSettings *p);
};

#endif // PORTCONFIG_H
