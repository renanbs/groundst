#include "portconfig.h"
#include "ui_portconfig.h"

PortConfig::PortConfig(QWidget *parent, Port *p) : QDialog(parent), ui(new Ui::PortConfig)
{
	ui->setupUi(this);
	uart = p;

	fillPortsParameters();

	connect (ui->comboBoxPort, SIGNAL (currentIndexChanged (int)), this, SLOT (showPortInfo (int)));

	fillPortsInfo ();
	loadSavedPortSettings ();
}

PortConfig::~PortConfig()
{
	delete ui;
}

void PortConfig::loadSavedPortSettings()
{
	uart->readPortSettings (&ps);
	int index;
	if ((index = ui->comboBoxPort->findText (ps.name, Qt::MatchExactly | Qt::MatchCaseSensitive)) != -1)
		ui->comboBoxPort->setCurrentIndex (index);
	if ((index = ui->comboBoxBaudRate->findText (ps.stringRate, Qt::MatchExactly | Qt::MatchCaseSensitive)) != -1)
		ui->comboBoxBaudRate->setCurrentIndex (index);
	if ((index = ui->comboBoxParity->findText (ps.stringParity, Qt::MatchExactly | Qt::MatchCaseSensitive)) != -1)
		ui->comboBoxParity->setCurrentIndex (index);
	if ((index = ui->comboBoxFlowControl->findText (ps.stringFlowControl, Qt::MatchExactly | Qt::MatchCaseSensitive)) != -1)
		ui->comboBoxFlowControl->setCurrentIndex (index);
	if ((index = ui->comboBoxDataBits->findText (ps.stringDataBits, Qt::MatchExactly | Qt::MatchCaseSensitive)) != -1)
		ui->comboBoxDataBits->setCurrentIndex (index);
	if ((index = ui->comboBoxStopBits->findText (ps.stringStopBits, Qt::MatchExactly | Qt::MatchCaseSensitive)) != -1)
		ui->comboBoxStopBits->setCurrentIndex (index);
}

void PortConfig::savePortSettings (pSettings *p)
{
	p->name = ui->comboBoxPort->currentText();
	p->rate = static_cast<QSerialPort::Parity>(ui->comboBoxBaudRate->itemData(ui->comboBoxBaudRate->currentIndex()).toInt());
	p->stringRate = ui->comboBoxBaudRate->currentText();
	p->parity = static_cast<QSerialPort::Parity>(ui->comboBoxParity->itemData(ui->comboBoxParity->currentIndex()).toInt());
	p->stringParity = ui->comboBoxParity->currentText();
	p->flowControl = static_cast<QSerialPort::FlowControl>(ui->comboBoxFlowControl->itemData(ui->comboBoxFlowControl->currentIndex()).toInt());
	p->stringFlowControl = ui->comboBoxFlowControl->currentText();
	p->dataBits = static_cast<QSerialPort::DataBits>(ui->comboBoxDataBits->itemData(ui->comboBoxDataBits->currentIndex()).toInt());
	p->stringDataBits = ui->comboBoxDataBits->currentText();
	p->stopBits = static_cast<QSerialPort::StopBits>(ui->comboBoxStopBits->itemData(ui->comboBoxStopBits->currentIndex()).toInt());
	p->stringStopBits = ui->comboBoxStopBits->currentText();
	uart->savePortSettings (p);
}

void PortConfig::on_buttonBox_accepted()
{
	savePortSettings (&ps);
}

void PortConfig::showPortInfo (int idx)
{
	if (idx != -1)
	{
		QStringList list = ui->comboBoxPort->itemData (idx).toStringList();
		ui->descriptionLabel->setText (tr ("Description: %1").arg (list.at (1)));
		ui->manufacturerLabel->setText (tr ("Manufacturer: %1").arg(list.at (2)));
		ui->locationLabel->setText (tr ("Location: %1").arg (list.at (3)));
		ui->vidLabel->setText (tr ("Vendor ID: %1").arg (list.at(4)));
		ui->pidLabel->setText (tr ("Product ID: %1").arg (list.at(5)));
	}
}

void PortConfig::fillPortsInfo ()
{
	ui->comboBoxPort->clear();
	foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts ())
	{
		QStringList list;
		list << info.portName () << info.description ()
			 << info.manufacturer () << info.systemLocation ()
			 << (info.hasVendorIdentifier() ? QString::number (info.vendorIdentifier (), 16) : QString ())
			 << (info.hasProductIdentifier() ?QString::number (info.productIdentifier (), 16) : QString ());

		ui->comboBoxPort->addItem (list.first (), list);
	}
}

void PortConfig::fillPortsParameters()
{
	ui->comboBoxBaudRate->addItem(QLatin1String("1200"), QSerialPort::Baud1200);
	ui->comboBoxBaudRate->addItem(QLatin1String("2400"), QSerialPort::Baud2400);
	ui->comboBoxBaudRate->addItem(QLatin1String("4800"), QSerialPort::Baud4800);
	ui->comboBoxBaudRate->addItem(QLatin1String("9600"), QSerialPort::Baud9600);
	ui->comboBoxBaudRate->addItem(QLatin1String("19200"), QSerialPort::Baud19200);
	ui->comboBoxBaudRate->addItem(QLatin1String("38400"), QSerialPort::Baud38400);
	ui->comboBoxBaudRate->addItem(QLatin1String("57600"), QSerialPort::Baud57600);
	ui->comboBoxBaudRate->addItem(QLatin1String("115200"), QSerialPort::Baud115200);

	// fill data bits
	ui->comboBoxDataBits->addItem(QLatin1String("5"), QSerialPort::Data5);
	ui->comboBoxDataBits->addItem(QLatin1String("6"), QSerialPort::Data6);
	ui->comboBoxDataBits->addItem(QLatin1String("7"), QSerialPort::Data7);
	ui->comboBoxDataBits->addItem(QLatin1String("8"), QSerialPort::Data8);
	ui->comboBoxDataBits->setCurrentIndex(3);

	// fill parity
	ui->comboBoxParity->addItem(QLatin1String("None"), QSerialPort::NoParity);
	ui->comboBoxParity->addItem(QLatin1String("Even"), QSerialPort::EvenParity);
	ui->comboBoxParity->addItem(QLatin1String("Odd"), QSerialPort::OddParity);
	ui->comboBoxParity->addItem(QLatin1String("Mark"), QSerialPort::MarkParity);
	ui->comboBoxParity->addItem(QLatin1String("Space"), QSerialPort::SpaceParity);

	// fill stop bits
	ui->comboBoxStopBits->addItem(QLatin1String("1"), QSerialPort::OneStop);
	#ifdef Q_OS_WIN
	ui->comboBoxStopBits->addItem(QLatin1String("1.5"), QSerialPort::OneAndHalfStop);
	#endif
	ui->comboBoxStopBits->addItem(QLatin1String("2"), QSerialPort::TwoStop);

	// fill flow control
	ui->comboBoxFlowControl->addItem(QLatin1String("None"), QSerialPort::NoFlowControl);
	ui->comboBoxFlowControl->addItem(QLatin1String("RTS/CTS"), QSerialPort::HardwareControl);
	ui->comboBoxFlowControl->addItem(QLatin1String("XON/XOFF"), QSerialPort::SoftwareControl);
}
