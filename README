groundST - Ground Station Serial Terminal

Implements a terminal program using QT and QExtSerialPort library. This terminal program mixes the input and output into one terminal
window, unlike other terminal programs which have separate input and output windows.  The intended use of the program is to
communicate with embedded systems over a serial port. The program builds on Windows and Linux.

:: Building:

	:: Linux/Windows:
		:: Get QExtSerialPort from http://code.google.com/p/qextserialport/.
		:: The directory structure should be like this:

		|-src/
			|-qextserialport/
			|-groundST/

		:: Using QTCreator, open the project in qextserialport/src (NOT the project in qextserialport/). That project builds some
		example programs that we don't need.  You should end up with a library file 	(libqextserialportd.a) or DLL
		(libqextserialportd.dll) on Windows.

		:: Now build the groundST project. You may need to adjust the paths in the groundST.pro file if the compilation fails.
		Especially on windows, where the qextserialport library filenames and paths seem to change of their own accord.

	:: Linux:
		:: Just open the groundST project with QTCreator and build it.

		:: The linux build of groundST can use a static library linking to qextserialport. This makes it possible to run the program
		without having to copy qextserialport.so into your system directory.  The QT 	libraries are still dynamically linked, but
		that isn't a problem for a linux system, since QT is already installed by the system.

		If the compile fails, it is probably a path error, where the compiler is unable to find the qextserialport include or library
		files. You 	may have to edit the paths inside the qst.pro file to correct this.

	:: Windows:
		:: Build the qextserialport project as explained earlier and locate the output .dll file and note its path and filename.

		:: Open the groundST (groundST.pro) project file. Edit to make the library path and filename match the dll filename from
		qextserialport.

		It may be tempting to try to build the program statically-linked to qextserialport. I was never able to get this to work,
		so on Windows, just use the qextserialport dll.

		To run the program outside of QTCreator, you will have to copy a bunch of DLL files to the same directory where the
		groundST.exe file is located. To find out what these dll files are, you can simply try to execute the groundST.exe file,
		and fix each "missing dll" error by copying the correct dll file into the same directory as the groundST.exe file.