#include "port.h"

Port::Port(QSerialPort *parent) : QSerialPort(parent)
{
	readPortSettings(&portSettings);
}

Port::~Port ()
{

}

void Port::readPortSettings (pSettings *p)
{
	QSettings settings ("groundST", "config");

	p->name = settings.value ("portname").toString();
	p->stringRate = settings.value ("stringRate").toString();
	p->rate = settings.value ("rate").toInt();
	p->stringDataBits = settings.value ("stringDataBits").toString();
	p->dataBits = (QSerialPort::DataBits) settings.value ("databits").toInt();
	p->stringStopBits = settings.value ("stringStopBits").toString();
	p->stopBits = (QSerialPort::StopBits) settings.value ("stopbits").toInt();
	p->stringFlowControl = settings.value ("stringFlowControl").toString();
	p->flowControl = (QSerialPort::FlowControl) settings.value ("flowControl").toInt();
}

void Port::savePortSettings (pSettings *p)
{
	portSettings = *p;
	QSettings settings ("groundST", "config");
	settings.setValue ("portname", p->name);
	settings.setValue ("stringRate", p->stringRate);
	settings.setValue ("rate", p->rate);
	settings.setValue ("stringDataBits", p->stringDataBits);
	settings.setValue ("databits", p->dataBits);
	settings.setValue ("stringStopBits", p->stringStopBits);
	settings.setValue ("stopbits", p->stopBits);
	settings.setValue ("stringFlowControl", p->stringFlowControl);
	settings.setValue ("flowControl", p->flowControl);
}

QSerialPort::SerialPortError Port::connectDisconnect()
{
	QSerialPort::SerialPortError ret = NoError;
	if (!isOpen())
	{
		setPortName (portSettings.name);
		if (open(QIODevice::ReadWrite))
		{
			if (setBaudRate (portSettings.rate)
				&& setDataBits (portSettings.dataBits)
				&& setParity (portSettings.parity)
				&& setStopBits (portSettings.stopBits)
				&& setFlowControl (portSettings.flowControl))
			{
				emit connectSP();
			}
			else
			{
				close();
				ret = error ();
			}
		}
		else
		{
			ret = error ();
		}
	}
	else
	{
		close ();
		emit disconnectSP ();
	}
	return ret;
}
