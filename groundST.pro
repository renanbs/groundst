TARGET = groundST
TEMPLATE = app
QT += widgets \
        serialport
SOURCES += main.cpp \
	mainwindow.cpp \
	portconfig.cpp \
	port.cpp \
	terminalstyle.cpp \
	terminalhistory.cpp \
	terminal.cpp \
	terminalsettings.cpp \
	terminalcommands.cpp
HEADERS += mainwindow.h \
	portconfig.h \
	port.h \
	terminalstyle.h \
	terminalhistory.h \
	terminal.h \
	terminalsettings.h \
	terminalcommands.h
FORMS += mainwindow.ui \
	about.ui \
	portconfig.ui \
	terminalstyle.ui \
	terminalsettings.ui
CONFIG += debug
RESOURCES += groundST.qrc
