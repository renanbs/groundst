#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include "qextserialport.h"
#include <QTimer>
#include <QFile>
#include <QAction>
#include <QLabel>
#include "ui_mainwindow.h"
#include "port.h"
#include "terminal.h"
#include "terminalsettings.h"

namespace Ui
{
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = 0);
		Ui::MainWindow *ui;
		~MainWindow ();

	public slots:
		void aboutQt ();
		void connectDisconnect ();
		void connectPort ();
		void disconnectPort ();
		void set_Tx_Led (bool on);
		void set_Rx_Led (bool on);

	private slots:
		void helpAbout ();
		void saveScreen ();
		void startLogging ();
		void endLogging ();
		void configureColors ();
		void configurePort ();
		void configureTerminal ();
		void quit ();
		void clearScreen ();
		void paste ();
		void selectAll ();
		void copy ();

	private:
		Terminal *term;
		void loadStyleConfiguration ();
		void createStatusBarLabels ();
		QWidgetList statusBarWidgetList;
		void populateSerialInfo ();
		void clearSerialInfo ();
		QLabel *_labelTx;
		QLabel *_labelRx;
		void createActions ();
		void addToolBarButtons ();
		QAction *conDiscAct;
		QAction *configureColorsAct;
		QAction *portConfigAct;
		QAction *configureTerminalAct;
		QAction *quitAct;
};

#endif // MAINWINDOW_H
