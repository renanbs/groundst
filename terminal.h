#ifndef TERMINAL_H
#define TERMINAL_H

#include <QPlainTextEdit>
#include <QString>
#include <QDateTime>
#include <QPoint>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include "terminalhistory.h"
#include "terminalcommands.h"
#include "port.h"
#include "portconfig.h"

class TerminalCommands;
class Terminal : public QPlainTextEdit
{
		Q_OBJECT
	public:
		explicit Terminal(QWidget *parent = 0);
		virtual ~Terminal ();
		void backspace ();
		void addToHistory ();
		void addTimeStamp ();
		void addTimeStampCR ();
		void nextOnCmdHistory ();
		void previousOnCmdHistory ();
		void updateCmdHistorydOnScreen ();
		QPoint getRowColumn ();
		QString getTypedCmd ();
		void updateTypedCmd (QString str);
		bool parseCommand (QString str);
		void listHistory ();
		void help ();
		bool treatBeforeSend (QString &str);
		void setAutoCrLf (bool option);
		void configurePort ();
		QSerialPort::SerialPortError connectDisconnect ();
		Port *getSerialPtr();

		QString getPortName ();
		QString getBaudRate ();
		QString getStopBits ();
		QString getDataBits ();
		QString getFlow ();
		QString getParity ();

	signals:
		void getData (const QByteArray &data);
		void setTxLed (bool on);
		void setRxLed (bool on);

	private slots:
		void readData ();
		void writeData (const QByteArray &data);

	public slots:
		void insertPlainText (const QString &text);
		void clearScreen ();
		void saveScreen ();
		void startLogging ();
		void endLogging ();
		void paste ();
		void copy ();
		void selectAll ();

	private:
		QPlainTextEdit *pText;
		TerminalHistory *termHist;
		TerminalCommands *termCom;
		Port *serial;

		bool eventFilter (QObject *obj, QEvent *event);
		QFile *logFile;
		QString cmd;
		void insertCmd ();
		void clearTypedCmd ();
		QString cmdHist;
		QString nextCmdHist;
		int stampLength;
		bool autocrlf;
};

#endif // TERMINAL_H
