#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "terminalstyle.h"
#include "portconfig.h"
#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <QTimer>
#include <QAction>
#include <QDateTime>
#include <QPalette>
#include "ui_about.h"
#include <QScopedPointer>


MainWindow::MainWindow (QWidget *parent) : QMainWindow (parent), ui (new Ui::MainWindow)//, uart(0)
{
	ui->setupUi (this);

	// Create the terminal widget
	term = new Terminal ();
	setCentralWidget (term);

	createActions ();
	addToolBarButtons ();
	createStatusBarLabels ();

	loadStyleConfiguration ();
	clearSerialInfo ();

	connect (term->getSerialPtr(), SIGNAL(connectSP()), this, SLOT (connectPort()));
	connect (term->getSerialPtr(), SIGNAL(disconnectSP()), this, SLOT (disconnectPort()));

	connect (term, SIGNAL(setRxLed(bool)), this, SLOT (set_Rx_Led (bool)));
	connect (term, SIGNAL(setTxLed(bool)), this, SLOT (set_Tx_Led (bool)));
}
MainWindow::~MainWindow()
{
}

void MainWindow::addToolBarButtons ()
{
	ui->toolBar->addAction (conDiscAct);
	ui->toolBar->addAction (portConfigAct);
	ui->toolBar->addAction (configureColorsAct);
	ui->toolBar->addAction (configureTerminalAct);
	ui->toolBar->addSeparator ();
	ui->toolBar->addAction (quitAct);
}

void MainWindow::createActions ()
{
	conDiscAct = new QAction (QIcon (":/img/images/disconnect.png"), tr ("Connect"), this);
	conDiscAct->setShortcut (tr ("F10"));
	conDiscAct->setStatusTip (tr ("Connect"));
	connect (conDiscAct, SIGNAL (triggered ()), this, SLOT (connectDisconnect()));

	portConfigAct = new QAction (QIcon (":/img/images/configure.png"), tr ("Configure Serial Port"), this);
	portConfigAct->setShortcut (tr ("Ctrl+1"));
	portConfigAct->setStatusTip (tr ("Configure Serial Port"));
	connect (portConfigAct, SIGNAL (triggered ()), this, SLOT (configurePort ()));

	configureColorsAct = new QAction (QIcon (":/img/images/palette.png"), tr ("Terminal Style"), this);
	configureColorsAct->setShortcut (tr ("Ctrl+2"));
	configureColorsAct->setStatusTip (tr ("Terminal Style"));
	connect (configureColorsAct, SIGNAL (triggered ()), this, SLOT (configureColors ()));

	configureTerminalAct = new QAction (QIcon (":/img/images/preferences.png"), tr ("Terminal Settings"), this);
	configureTerminalAct->setShortcut (tr ("Ctrl+3"));
	configureTerminalAct->setStatusTip (tr ("Terminal Settings"));
	connect (configureTerminalAct, SIGNAL (triggered ()), this, SLOT (configureTerminal ()));

	quitAct = new QAction (QIcon (":/img/images/exit.png"), tr ("Exit groundST"), this);
	quitAct->setShortcut (tr ("Alt+F4"));
	quitAct->setStatusTip (tr ("Exit groundST"));
	connect (quitAct, SIGNAL (triggered ()), this, SLOT (quit ()));

	connect (ui->aboutAct, SIGNAL (triggered ()), this, SLOT (helpAbout ()));
	connect (ui->aboutQtAct, SIGNAL (triggered ()), this, SLOT (aboutQt ()));

	connect (ui->saveScreenAct, SIGNAL (triggered ()), this, SLOT (saveScreen ()));
	connect (ui->beginLogAct, SIGNAL (triggered ()), this, SLOT (startLogging ()));
	connect (ui->endLogAct, SIGNAL (triggered ()), this, SLOT (endLogging ()));

	connect (ui->selectAllAct, SIGNAL (triggered ()), this, SLOT (selectAll ()));
	connect (ui->copyAct, SIGNAL (triggered ()), this, SLOT (copy ()));
	connect (ui->pasteAct, SIGNAL (triggered ()), this, SLOT (paste ()));
	connect (ui->clearScreenAct, SIGNAL (triggered ()), this, SLOT (clearScreen ()));
}

void MainWindow::connectPort ()
{
	conDiscAct->setIcon (QIcon (":/img/images/connect.png"));

	conDiscAct->setStatusTip (tr ("Disconnect"));
	conDiscAct->setToolTip ( tr ("Disconnect"));
	conDiscAct->setText ( tr ("Disconnect"));

	populateSerialInfo ();
}

void MainWindow::disconnectPort ()
{
	conDiscAct->setIcon (QIcon (":/img/images/disconnect.png"));
	conDiscAct->setStatusTip (tr ("Connect"));
	conDiscAct->setToolTip( tr ("Connect"));
	conDiscAct->setText (tr ("Connect"));

	clearSerialInfo ();
	set_Rx_Led (false);
	set_Tx_Led (false);
}

void MainWindow::connectDisconnect()
{
	term->connectDisconnect ();
}

void MainWindow::configureColors ()
{
	TerminalStyle *conf = new TerminalStyle (this, term);
	conf->exec ();
	if (conf)
	{
		delete conf;
		conf = NULL;
	}
}

void MainWindow::configurePort ()
{
	term->configurePort();
}

void MainWindow::configureTerminal ()
{
	QScopedPointer <TerminalSettings> ts (new TerminalSettings (this, term));
	ts->exec ();
}

void MainWindow::quit()
{
	QCoreApplication::quit();
}

void MainWindow::helpAbout ()
{
	QDialog dlg;
	Ui_aboutDialog dlg_ui;
	dlg_ui.setupUi(&dlg);

	dlg.exec();
}

void MainWindow::saveScreen ()
{
	term->saveScreen ();
}

void MainWindow::startLogging ()
{
	term->startLogging ();
}

void MainWindow::endLogging ()
{
	term->endLogging ();
}

void MainWindow::clearScreen ()
{
	term->clearScreen ();
	term->addTimeStamp();
}

void MainWindow::copy()
{
	term->copy ();
}

void MainWindow::paste ()
{
	term->paste ();
}

void MainWindow::selectAll ()
{
	term->selectAll ();
}

void MainWindow::aboutQt ()
{
	QMessageBox::aboutQt (this, "About Qt");
}

void MainWindow::loadStyleConfiguration ()
{
	QPalette pal = term->palette ();
	QFont font ("Courier New", 10);
	QColor backColor;
	QColor fontColor;
	QSettings settings ("groundST", "style");

	/// If exists at least one setting, testing for background color
	if (settings.contains ("backColor"))
	{
		backColor = settings.value ("backColor").value<QColor>();
		fontColor = settings.value ("fontColor").value<QColor>();
		font = settings.value ("font").value<QFont>();
	}
	else
	{
		backColor = Qt::black;
		fontColor = Qt::white;
	}
	pal.setColor (QPalette::Base, backColor);
	pal.setColor (QPalette::Text, fontColor);

	term->setPalette (pal);
	term->setAutoFillBackground (true);
	term->setFont (font);
}

void MainWindow::createStatusBarLabels ()
{
	_labelTx = new QLabel (this);
	_labelTx->setMinimumSize (QSize (15, 15));
	_labelTx->setMaximumSize (QSize (15, 15));
	_labelTx->setPixmap (QPixmap (QString::fromUtf8 (":/img/images/ledyellow.png")));
	_labelTx->setScaledContents (true);
	QSizePolicy sizePolicyTx (QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicyTx.setHorizontalStretch (0);
	sizePolicyTx.setVerticalStretch (0);
	sizePolicyTx.setHeightForWidth (_labelTx->sizePolicy().hasHeightForWidth ());
	_labelTx->setSizePolicy (sizePolicyTx);

	_labelRx = new QLabel (this);
	_labelRx->setMinimumSize (QSize (15, 15));
	_labelRx->setMaximumSize (QSize (15, 15));
	_labelRx->setPixmap (QPixmap (QString::fromUtf8 (":/img/images/ledyellow.png")));
	_labelRx->setScaledContents (true);
	QSizePolicy sizePolicyRx (QSizePolicy::Fixed, QSizePolicy::Fixed);
	sizePolicyRx.setHorizontalStretch (0);
	sizePolicyRx.setVerticalStretch (0);
	sizePolicyRx.setHeightForWidth (_labelRx->sizePolicy().hasHeightForWidth ());
	_labelRx->setSizePolicy (sizePolicyRx);

	statusBarWidgetList.append (_labelTx);
	statusBarWidgetList.append (_labelRx);

	statusBarWidgetList.at (0)->hide();
	statusBarWidgetList.at (1)->hide();

	int loop;
	for (loop = 0; loop < 6; loop++)
	{
		// Put a label in each box
		QLabel *label = new QLabel ();
		label->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
		statusBarWidgetList.append (label);
	}
}

void MainWindow::populateSerialInfo ()
{
	QStringList tmp;

	int loop;
	int len = statusBarWidgetList.size ();
	for (loop = 0; loop < len; loop++)
	{
		statusBarWidgetList.at (loop)->hide();
		ui->statusBar->removeWidget (statusBarWidgetList.at (loop));
	}

	for (loop = 0; loop < len; loop++)
	{
		ui->statusBar->addWidget (statusBarWidgetList.at (loop));
		statusBarWidgetList.at (loop)->show();
	}

	QLabel *label;
	label = static_cast<QLabel*> (statusBarWidgetList.at (2));
	label->setText (tr (" :: Port: ") + term->getPortName());
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (3));
	label->setText (tr (" :: Baud: ") + term->getBaudRate());
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (4));
	label->setText (tr (" :: Data: ") + term->getDataBits());
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (5));
	label->setText (tr (" :: Parity: ") + term->getParity());
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (6));
	label->setText (tr (" :: Stop: ") + term->getStopBits());
	label->setFixedWidth (label->minimumSizeHint ().width ());

	label = static_cast<QLabel*> (statusBarWidgetList.at (7));
	label->setText (tr (" :: Flow: ") + term->getFlow());
	label->setFixedWidth (label->minimumSizeHint ().width ());
}

void MainWindow::clearSerialInfo ()
{
	int loop;
	int len = statusBarWidgetList.size ();
	for (loop = 0; loop < len; loop++)
	{
		statusBarWidgetList.at (loop)->hide();
		ui->statusBar->removeWidget (statusBarWidgetList.at (loop));
	}
}

void MainWindow::set_Tx_Led (bool on)
{
	if (on)
		_labelTx->setPixmap (QPixmap (":/img/images/ledgreen.png"));
	else
		_labelTx->setPixmap (QPixmap (":/img/images/ledred.png"));

}

void MainWindow::set_Rx_Led (bool on)
{
	if (on)
		_labelRx->setPixmap (QPixmap (":/img/images/ledgreen.png"));
	else
		_labelRx->setPixmap (QPixmap (":/img/images/ledred.png"));
}
