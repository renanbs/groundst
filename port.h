#ifndef PORT_H
#define PORT_H

#include <QObject>
#include <QStringList>
#include <QSettings>
#include <QDir>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

struct __portSettings
{
	QString name;
	qint32 rate;
	QString stringRate;
	QSerialPort::DataBits dataBits;
	QString stringDataBits;
	QSerialPort::Parity parity;
	QString stringParity;
	QSerialPort::StopBits stopBits;
	QString stringStopBits;
	QSerialPort::FlowControl flowControl;
	QString stringFlowControl;
};
typedef __portSettings pSettings;

class Port : public QSerialPort
{
	Q_OBJECT

	public:
		explicit Port (QSerialPort *parent = 0);
		virtual ~Port();

		QString getPortName ();

		void readPortSettings (pSettings *p);
		void savePortSettings (pSettings *p);
		SerialPortError connectDisconnect ();

	signals:
		void connectSP ();
		void disconnectSP ();

	public slots:

	private:
		pSettings portSettings;
};

#endif // PORT_H
