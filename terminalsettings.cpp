#include "terminalsettings.h"
#include "ui_terminalsettings.h"
#include <QSettings>

TerminalSettings::TerminalSettings(QWidget *parent, Terminal *t) : QDialog(parent), ui (new Ui::TerminalSettings)
{
	ui->setupUi (this);
	term = t;
	loadConfiguration ();
}

TerminalSettings::~TerminalSettings()
{
	delete ui;
}

void TerminalSettings::loadConfiguration ()
{
	QSettings settings (QSettings::UserScope, "groundST", "history");
	QString histSize = settings.value ("historySize").toString ();
	if (!histSize.isEmpty ())
		ui->lineEditHistorySize->setText (histSize);
	else
		ui->lineEditHistorySize->setText ("100");

	QSettings s (QSettings::UserScope, "groundST", "settings");
	autocrlf = s.value ("autoCRLF").toBool ();
	ui->checkBoxAutoCRLF->setChecked (autocrlf);
}

void TerminalSettings::on_buttonBox_accepted()
{
	QSettings settings (QSettings::UserScope, "groundST", "history");
	if (!ui->lineEditHistorySize->text().isEmpty())
		settings.setValue ("historySize", ui->lineEditHistorySize->text());
	else
		settings.setValue ("historySize", "100");

	QSettings s (QSettings::UserScope, "groundST","settings");
	autocrlf = ui->checkBoxAutoCRLF->isChecked ();
	s.setValue ("autoCRLF", autocrlf);
	term->setAutoCrLf (autocrlf);
}
